build: api_lambda.zip alexa_lambda.zip
clean_api_lambda_zip:
	rm api_lambda.zip || true
clean_alexa_lambda_zip:
	rm alexa_lambda.zip || true
clean: clean_api_lambda_zip clean_alexa_lambda_zip
	cargo clean
api_lambda.zip: clean_api_lambda_zip
	cargo build --target=x86_64-unknown-linux-musl --release
	cp target/x86_64-unknown-linux-musl/release/api ./bootstrap
	zip api_lambda.zip bootstrap
	rm bootstrap
alexa_lambda.zip: clean_alexa_lambda_zip
	cargo build --target=x86_64-unknown-linux-musl --release
	cp target/x86_64-unknown-linux-musl/release/alexa ./bootstrap
	zip alexa_lambda.zip bootstrap
	rm bootstrap
