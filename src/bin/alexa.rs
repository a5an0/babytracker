extern crate alexa_sdk;
extern crate lambda_runtime as lambda;
extern crate rusoto_core;
extern crate rusoto_dynamodb;

use alexa_sdk::request::IntentType;
use alexa_sdk::{Request, Response};
use lambda::{error::HandlerError, lambda, Context};
use std::convert::From;
use std::error::Error;

use common::{add_feeding, get_last_feeding, get_total_for_today};

fn main() -> Result<(), Box<dyn Error>> {
    lambda!(my_handler);

    Ok(())
}

fn my_handler(req: Request, _ctx: Context) -> Result<Response, HandlerError> {
    let user_id = get_user_id(&req);
    match req.intent() {
        IntentType::User(s) => match s.as_ref() {
            "add_feeding" => {
                let mut amount: i32 = req
                    .slot_value("amount")
                    .expect("No amount provided")
                    .parse()
                    .unwrap();
                let units = match req.slot_value("unit") {
                    Some(s) => s,
                    None => String::from("millileters"),
                };
                if units == "ounces" {
                    amount *= 30;
                }
                add_feeding(&user_id, amount)
                    .map(|m| Response::from(m))
                    .map_err(|e| HandlerError::from(e.as_ref()))
            }
            "get_last_feeding" => get_last_feeding(&user_id)
                .map(|m| Response::from(m))
                .map_err(|e| HandlerError::from(e.as_ref())),
            "get_todays_total" => get_total_for_today(&user_id)
                .map(|m| Response::from(m))
                .map_err(|e| HandlerError::from(e.as_ref())),
            &_ => handle_cancel(&req),
        },
        _ => handle_cancel(&req),
    }
}

fn handle_cancel(_req: &Request) -> Result<Response, HandlerError> {
    println!("Cancelling request");
    Ok(Response::simple(
        "help",
        "You can say things like he ate 30 mills, or when was the last feeding",
    ))
}

fn get_user_id(req: &Request) -> String {
    let session = &req.session.clone().unwrap();
    session.user.user_id.to_string()
}
