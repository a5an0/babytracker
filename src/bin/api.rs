use lambda_http::{lambda, IntoResponse, Request, Response};
use lambda_runtime::{error::HandlerError, Context};
use serde::{Deserialize, Serialize};
use serde_json;
use serde_json::Value;
use std::fmt::Debug;

use common::{add_feeding, get_feedings_for_today, get_last_feeding, get_total_for_today, Message};

static MY_USER_ID: &str = "amzn1.ask.account.AEFXCM4PMKDYPCCJYZDU2TQWHVSUIFZRVALIGUOKV4TSGNHARAC5IJDNRBC6B7GGHPVQXOFVH2HDLFQYSYHG7RQIBMEBX2I465PW2INJWROSC3SUY5M62IO7MUZ5C5FEMLGCYMJXJEXP44NQIXFIA32VJSAUZ7ORWNEYYDEMFF5UBNJTJHLWKK7Z4GIFUYF3EJYF4KIZLETAUDQ";

fn main() {
    lambda!(hello)
}

#[derive(Serialize, Deserialize, Debug)]
struct FeedingList {
    feedings: Vec<(String, i32)>,
}

fn build_response<M: Serialize + Debug>(msg: M) -> Response<String> {
    println!("Building resonse for message: {:?}", msg);
    let mut resp = Response::new(serde_json::to_string(&msg).unwrap());
    resp.headers_mut()
        .insert("Access-Control-Allow-Origin", "*".parse().unwrap());
    return resp;
}

fn hello(request: Request, _ctx: Context) -> Result<impl IntoResponse, HandlerError> {
    println!("method: {:?}", &request.method().as_ref());
    match request.uri().path() {
        "/feeding" => match request.method().as_ref() {
            "GET" => match get_last_feeding(MY_USER_ID) {
                Ok(msg) => Ok(build_response(msg)),
                Err(e) => Err(HandlerError::from(e.as_ref())),
            },
            "POST" => {
                let body: Value = serde_json::from_slice(request.body())?;
                // TODO: change the signature for add_feeding to take an i64
                match add_feeding(MY_USER_ID, body["amount"].as_i64().unwrap() as i32) {
                    Ok(msg) => Ok(build_response(msg)),
                    Err(e) => Err(HandlerError::from(e.as_ref())),
                }
                //                Ok(format!("The amount is {}", body["amount"]))
            }
            _ => Ok(build_response(Message::new("", ""))),
        },
        "/today" => match get_total_for_today(MY_USER_ID) {
            Ok(msg) => Ok(build_response(msg)),
            Err(e) => Err(HandlerError::from(e.as_ref())),
        },
        "/today_data" => match get_feedings_for_today(MY_USER_ID) {
            Ok(res) => Ok(build_response(FeedingList { feedings: res })),
            Err(e) => Err(HandlerError::from(e.as_ref())),
        },
        &_ => Ok(build_response(Message::new("", ""))),
    }

    //Ok(serde_json::to_string(&Message::new("hello", "there")).unwrap())
}
