extern crate rusoto_core;
extern crate rusoto_dynamodb;

use alexa_sdk::Response;
use chrono::prelude::*;
use chrono_tz::US::Eastern;
use rusoto_core::{Region, RusotoResult};
use rusoto_dynamodb::{
    AttributeValue, DynamoDb, DynamoDbClient, PutItemError, PutItemInput, PutItemOutput, QueryInput,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Deserialize, Serialize, Debug)]
pub struct Message {
    title: String,
    message: String,
}

impl Message {
    pub fn new(title: &str, message: &str) -> Self {
        Self {
            title: title.to_owned(),
            message: message.to_owned(),
        }
    }
    pub fn get_title(&self) -> String {
        self.title.clone()
    }
    pub fn get_message(&self) -> String {
        self.message.clone()
    }
}

impl From<Message> for Response {
    fn from(message: Message) -> Self {
        Self::simple(&message.get_title(), &message.get_message())
    }
}

fn get_feedings_for_day(user_id: &str, datestamp: &str) -> Result<Vec<(String, i32)>, String> {
    println!("Got to get_feedings_for_day");
    let client = DynamoDbClient::new(Region::UsEast1);
    let mut expr_vals = HashMap::new();
    expr_vals.insert(
        ":user_id".to_string(),
        AttributeValue {
            s: Some(user_id.to_string()),
            ..Default::default()
        },
    );
    expr_vals.insert(
        ":today".to_string(),
        AttributeValue {
            s: Some(datestamp.to_string()),
            ..Default::default()
        },
    );
    let query_input = QueryInput {
        table_name: "baby_feeding".to_string(),
        key_condition_expression: Some(
            "user_id = :user_id AND begins_with(ts, :today)".to_string(),
        ),
        expression_attribute_values: Some(expr_vals),
        ..Default::default()
    };
    match client.query(query_input).sync() {
        Ok(output) => {
            println!("output.items: {:?}", output.items);
            match output.items {
                Some(items) => Ok(items
                    .iter()
                    .map(|item| {
                        let ts = item.get("ts").unwrap().clone().s.unwrap();
                        let amount: i32 = item
                            .get("amount")
                            .unwrap()
                            .clone()
                            .n
                            .unwrap()
                            .parse()
                            .unwrap();
                        (ts, amount)
                    })
                    .collect()),
                None => Ok(vec![]),
            }
        }
        Err(err) => {
            println!("Error fetching today's feedings: {}", err);
            Err(String::from("Was not able to get today's feedings"))
        }
    }
}

pub fn get_feedings_for_today(user_id: &str) -> Result<Vec<(String, i32)>, String> {
    let now = get_now_ts();
    let today = now.split("T").collect::<Vec<&str>>()[0];
    get_feedings_for_day(user_id, &today)
}

pub fn get_total_for_today(user_id: &str) -> Result<Message, String> {
    println!("Got to get_total_for_today!");
    let now = get_now_ts();
    let today = now.split("T").collect::<Vec<&str>>()[0];
    get_feedings_for_day(user_id, &today).map(|feedings| {
        let total = feedings.iter().fold(0, |acc, feeding| acc + feeding.1);
        Message::new(
            "Feedings so far",
            &format!(
                "So far, he has eaten {} millileters or {} ounces today.",
                total,
                total / 30
            ),
        )
    })
}

pub fn get_last_feeding(user_id: &str) -> Result<Message, String> {
    println!("Got to get_last_feeding!");
    let client = DynamoDbClient::new(Region::UsEast1);
    let mut expr_vals = HashMap::new();
    expr_vals.insert(
        ":user_id".to_string(),
        AttributeValue {
            s: Some(user_id.to_string()),
            ..Default::default()
        },
    );
    let query_input = QueryInput {
        table_name: "baby_feeding".to_string(),
        key_condition_expression: Some("user_id = :user_id".to_string()),
        expression_attribute_values: Some(expr_vals),
        limit: Some(1),
        scan_index_forward: Some(false),
        ..Default::default()
    };
    match client.query(query_input).sync() {
        Ok(output) => match output.items {
            Some(items) => {
                if items.len() == 0 {
                    return Ok(Message::new("None Found", "I didn't find a recent feeding"));
                }
                let first = &items[0];
                let t = first.get("ts").unwrap();
                let t_val = t.s.clone().unwrap();
                let parsed_timestamp = DateTime::parse_from_rfc3339(&t_val).unwrap();
                let formatted_time = parsed_timestamp.format("%B %e at %l:%M");
                let a = first.get("amount").unwrap();
                let amount = a.n.clone().unwrap();
                Ok(Message::new(
                    "Last feeding",
                    &format!(
                        "The last feeding was on {}. He ate {} millileters",
                        formatted_time, amount
                    ),
                ))
            }
            None => Ok(Message::new("None Found", "I didn't find a recent feeding")),
        },
        Err(err) => {
            println!("Could not query last feeding: {}", err);
            Err(String::from("There was an error fetching the last feeding"))
        }
    }
}

pub fn add_feeding(user_id: &str, amount: i32) -> Result<Message, String> {
    println!("Got to add_feeding!");
    match store_feeding(&user_id, &get_now_ts(), amount) {
        Ok(_) => {
            let response_str = format!("I recorded that he ate {} milliliters", amount);
            Ok(Message::new("Amount Saved!", &response_str))
        }
        Err(err) => {
            println!("Problem writing amount to the DB: {}", err);
            Err(String::from("There was a problem saving the amount"))
        }
    }
}

fn store_feeding(
    user_id: &str,
    timestamp: &str,
    amount: i32,
) -> RusotoResult<PutItemOutput, PutItemError> {
    let client = DynamoDbClient::new(Region::UsEast1);
    let mut item = HashMap::new();
    item.insert(
        "user_id".to_string(),
        AttributeValue {
            s: Some(user_id.to_string()),
            ..Default::default()
        },
    );
    item.insert(
        "ts".to_string(),
        AttributeValue {
            s: Some(timestamp.to_string()),
            ..Default::default()
        },
    );
    item.insert(
        "amount".to_string(),
        AttributeValue {
            n: Some(amount.to_string()),
            ..Default::default()
        },
    );
    let put_input = PutItemInput {
        table_name: "baby_feeding".to_string(),
        item: item,
        ..Default::default()
    };
    client.put_item(put_input).sync()
}

fn get_now_ts() -> String {
    let utc: DateTime<Utc> = Utc::now();
    let now = utc.with_timezone(&Eastern);
    now.to_rfc3339()
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
